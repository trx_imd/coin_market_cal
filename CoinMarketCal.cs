﻿using AngleSharp.Dom;
using AngleSharp.Dom.Html;
using AngleSharp.Parser.Html;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace CoinMarketCalParser
{
    public class CoinMarketCal
    {
        public List<Dictionary<string, string>> GetInfo(int startpage, int endpage)
        {
            List<Dictionary<string, string>> events = new List<Dictionary<string, string>>();

            for (int i = startpage; i <= endpage; i++)
            {
                string baseurl = "http://coinmarketcal.com/";
                var content = GetRequest(baseurl + "?page="+i);
                var parser = new HtmlParser();

                var doc = parser.Parse(content);
                var nodes = doc.QuerySelectorAll("article");
                foreach (var node in nodes)
                {
                    Dictionary<string, string> eventinfo = new Dictionary<string, string>();
                    var headers = node.QuerySelectorAll("h5");
                    string date = headers[0].TextContent.Replace("\n", "").Replace("  ", ""),
                        token = headers[1].TextContent.Replace("\n", "").Replace("  ", ""),
                        eventname = headers[2].TextContent.Replace("\n", "").Replace("  ", ""),
                        uid = date + " " + token + " " + eventname;
                    eventinfo.Add("Date", date);
                    eventinfo.Add("Token", token);
                    eventinfo.Add("EventName", eventname);
                    var descr = node.QuerySelectorAll("p").Where(q => q.ClassName == "description").FirstOrDefault();
                    eventinfo.Add("Description", descr.TextContent);
                    var proofnsource = node.QuerySelectorAll("a").Where(q => q.ClassName == "btn btn-w btn-xs btn-round");
                    eventinfo.Add("ProofUrl", baseurl + proofnsource.Where(q => q.TextContent.Contains(" Proof")).FirstOrDefault().Attributes["href"].Value);
                    if (proofnsource.Count() == 1)
                    {
                        eventinfo.Add("SourceUrl", null);
                    }
                    else
                    {
                        eventinfo.Add("SourceUrl", proofnsource.Where(q => q.TextContent.Contains(" Source")).FirstOrDefault().Attributes["href"].Value);
                    }
                    string rawvalid = node.QuerySelectorAll("span").Where(q => q.ClassName == "votes").FirstOrDefault().TextContent;
                    eventinfo.Add("Validation", rawvalid.Remove(0, 1).Substring(0, rawvalid.IndexOf(' ')));
                    eventinfo.Add("UID", uid);
                    events.Add(eventinfo);
                }

            }
            return events;
        }
        public string GetRequest(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            //WebHeaderCollection header = response.Headers;
            string responseText;
            using (var reader = new StreamReader(response.GetResponseStream()))
            {
                responseText = reader.ReadToEnd();
            }
            return responseText;
        }
    }
}
